<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeComentarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_comentars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
        });

        Schema::table('like_comentars', function (Blueprint $table) {
            //
            $table->unsignedBigInteger('user_id');

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('like_comentars', function (Blueprint $table) {
            //
            $table->unsignedBigInteger('comentar_id');

            $table->foreign('comentar_id')->references('id')->on('comentars');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_comentars');

        Schema::table('like_comentars', function (Blueprint $table) {
            //
            $table->dropForeign(['user_id']);
            $table->dropColumn(['user_id']);
        });
        Schema::table('like_comentars', function (Blueprint $table) {
            //
            $table->dropForeign(['comentar_id']);
            $table->dropColumn(['comentar_id']);
        });
    }
}
