<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likes', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->timestamps();
        });

        Schema::table('likes', function (Blueprint $table) {
            //
            $table->unsignedBigInteger('user_id');

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('likes', function (Blueprint $table) {
            //
            $table->unsignedBigInteger('post_id');

            $table->foreign('post_id')->references('id')->on('posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likes');

        Schema::table('comentar', function (Blueprint $table) {
            //
            $table->dropForeign(['user_id']);
            $table->dropColumn(['user_id']);
        });
        Schema::table('comentar', function (Blueprint $table) {
            //
            $table->dropForeign(['post_id']);
            $table->dropColumn(['post_id']);
        });
    }
}
